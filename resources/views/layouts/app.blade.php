<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  @include('includes.link')

</head>

<body>

  <!-- Navigation -->
  @include('includes.menu')

  <!-- Page Header -->
  
  @include('includes.header')
  <!-- Main Content -->
  <div class="container">
    <div class="row">
        @yield('content')
      
    </div>
  </div>
  <hr>

  <!-- Footer -->
  @include('includes.footer')

  <!-- Bootstrap core JavaScript -->
  @include('includes.script')

</body>

</html>
