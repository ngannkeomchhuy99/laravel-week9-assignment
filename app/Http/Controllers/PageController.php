<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function aboutPage(){
        return view('pages.about');
    }
    public function contactPage(){
        return view('pages.contact');
    }
}

